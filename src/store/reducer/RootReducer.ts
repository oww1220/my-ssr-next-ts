import { combineReducers } from 'redux';

import { LoadReducer, ILoadState } from '@src/store/reducer/LoadReducer';
import { ErrorReducer, IErrorState } from '@src/store/reducer/ErrorReducer';
import { LogInReducer, ILogInState } from '@src/store/reducer/LogInReducer';
import { GetTestListReducer, IGetTestListState } from '@src/store/reducer/GetTestListReducer';

export type State = {
    LoadReducer: ILoadState;
    ErrorReducer: IErrorState;
    LogInReducer: ILogInState;
    GetTestListReducer: IGetTestListState;
};

const rootReducer = combineReducers<State>({
    LoadReducer,
    ErrorReducer,
    LogInReducer,
    GetTestListReducer,
});

export default rootReducer;
