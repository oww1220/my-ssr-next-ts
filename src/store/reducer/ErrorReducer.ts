import { HYDRATE } from 'next-redux-wrapper';
import { State } from '@src/store/reducer/RootReducer';
import { GraphQLError } from 'graphql';
import * as Interface from '@src/interfaces/Interfaces';

/*액션상수*/
export const FAILURE_LOAD = 'ErrorReducer/FAILURE_LOAD' as const;

/*액션함수*/
export const failureLoad = (error: GraphQLError[], errorCallBack: () => any | null = null) => ({
    type: FAILURE_LOAD,
    payload: { error, errorCallBack },
});

/*기본상태 및 리듀서*/
export interface IErrorState {
    errorData: GraphQLError[] | null;
    errorCallBack: () => any | null;
}

const initialState = {
    errorData: null,
    errorCallBack: null,
};

type ErrorAction = ReturnType<typeof failureLoad>;

export function ErrorReducer(
    state: IErrorState = initialState,
    action: ErrorAction | { type: typeof HYDRATE; payload: State },
) {
    //console.log('action.type!!!', action);
    switch (action.type) {
        case HYDRATE:
            return {
                ...state,
                ...action.payload.ErrorReducer,
            };

        case FAILURE_LOAD:
            return {
                ...state,
                errorData: action.payload.error,
                errorCallBack: action.payload.errorCallBack,
            };

        default:
            return state;
    }
}
