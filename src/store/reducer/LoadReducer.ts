import { HYDRATE } from 'next-redux-wrapper';
import { State } from '@src/store/reducer/RootReducer';

/*액션상수*/
export const SHOW_LOAD = 'LoadReducer/SHOW_LOAD' as const;
export const HIDE_LOAD = 'LoadReducer/HIDE_LOAD' as const;

/*액션함수*/
export const showLoad = () => ({ type: SHOW_LOAD });
export const hideLoad = () => ({ type: HIDE_LOAD });

/*기본상태 및 리듀서*/
export interface ILoadState {
    loadState: boolean;
}

const initialState = {
    loadState: false,
};

type LoadAction = ReturnType<typeof showLoad> | ReturnType<typeof hideLoad>;

export function LoadReducer(
    state: ILoadState = initialState,
    action: LoadAction | { type: typeof HYDRATE; payload: State },
) {
    switch (action.type) {
        case HYDRATE:
            return {
                ...state,
                ...action.payload.LoadReducer,
            };

        case SHOW_LOAD:
            return {
                ...state,
                loadState: true,
            };

        case HIDE_LOAD:
            return {
                ...state,
                loadState: false,
            };

        default:
            return state;
    }
}
