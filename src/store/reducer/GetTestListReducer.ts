import { HYDRATE } from 'next-redux-wrapper';
import { State } from '@src/store/reducer/RootReducer';
import * as Interface from '@src/interfaces/Interfaces';

/*액션상수*/
export const FETCH_TESTLIST = 'GetTestListReducer/FETCH_TESTLIST' as const;
export const TESTLIST_SUCCESS = 'GetTestListReducer/TESTLIST_SUCCESS' as const;

export const FETCH_AUTHLIST = 'GetTestListReducer/FETCH_AUTHLIST' as const;
export const AUTHLIST_SUCCESS = 'GetTestListReducer/AUTHLIST_SUCCESS' as const;
export const AUTHLIST_RESET = 'GetTestListReducer/AUTHLIST_RESET' as const;

/*액션함수*/
export const fetchTestList = () => ({
    type: FETCH_TESTLIST,
});
export const fetchTestSuccess = (testLists: Interface.IUsersData[]) => ({
    type: TESTLIST_SUCCESS,
    payload: { testLists },
});

export const fetchAuthList = () => ({
    type: FETCH_AUTHLIST,
});
export const fetchAuthSuccess = (authObj: Interface.IAuthData) => ({
    type: AUTHLIST_SUCCESS,
    payload: { authObj },
});
export const authListReset = () => ({
    type: AUTHLIST_RESET,
});

/*기본상태 및 리듀서*/
export interface IGetTestListState {
    testLists: Interface.IUsersData[];
    authObj: Interface.IAuthData | null;
}
export const initialState = {
    testLists: [],
    authObj: null,
};

type GetTestListAction =
    | ReturnType<typeof fetchTestList>
    | ReturnType<typeof fetchTestSuccess>
    | ReturnType<typeof fetchAuthList>
    | ReturnType<typeof fetchAuthSuccess>
    | ReturnType<typeof authListReset>;

export function GetTestListReducer(
    state: IGetTestListState = initialState,
    action: GetTestListAction | { type: typeof HYDRATE; payload: State },
) {
    //console.log('action.type!!!', action);
    switch (action.type) {
        //next-redux-wrapper기준!!
        //next.js에서 생성한 redux store와 client에서 생성한 redux store는 다르기 때문에 이 둘을 합쳐야 한다.
        //그래서 이렇게 서버에서 생성한 스토어의 상태를 HYDRATE라는 액션을 통해서 클라이언트에 합쳐주는 작업이 필요한것이다.
        //action.payload에는 서버에서 생성한 스토어의 상태가 담겨있다. 이 둘을 합쳐 새로운 클라이언트의 리덕스 스토어의 상태를 만든다.
        case HYDRATE:
            return {
                ...state,
                ...action.payload.GetTestListReducer,
            };

        case FETCH_TESTLIST:
            return {
                ...state,
            };

        case TESTLIST_SUCCESS:
            return {
                ...state,
                testLists: action.payload.testLists,
            };

        case AUTHLIST_SUCCESS:
            return {
                ...state,
                authObj: action.payload.authObj,
            };

        case AUTHLIST_RESET:
            return {
                ...state,
                authObj: null,
            };

        default:
            return state;
    }
}
