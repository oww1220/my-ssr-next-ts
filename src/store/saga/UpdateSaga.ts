import { takeEvery, put, call, select, delay } from 'redux-saga/effects';
import * as ActionLogIn from '@src/store/reducer/LogInReducer';
import * as ActionLoad from '@src/store/reducer/LoadReducer';
import * as ActionError from '@src/store/reducer/ErrorReducer';
import * as ActionTestList from '@src/store/reducer/GetTestListReducer';
import * as Api from '@src/lib/Api';
import * as Commons from '@src/lib/Commons';
import Config from '@src/lib/Config';
import Router from 'next/router';

function* UpdateUserSaga(action: ReturnType<typeof ActionLogIn.updateUser>): Generator<any, void, any> {
    console.log('saga!:', action);

    const { data } = action.payload;

    yield put(ActionLoad.showLoad());

    try {
        const { data: graphQlData } = yield call(Api.updateUser, data);
        //console.log( graphQlData.data.updateUser);

        //graphQl api 에러체크!: graphQl error array를 넘김!
        if (graphQlData.errors?.length) {
            yield put(ActionError.failureLoad(graphQlData.errors));
            yield put(ActionLoad.hideLoad());
            return;
        }

        /*업데이트 정보 리덕스 스토어에 저장*/
        yield put(ActionLogIn.updateUserSuccess(graphQlData.data.updateUser));

        /*token 정보 새로 업데이트*/
        const { data: token } = yield call(Api.resetToken, data);
        //console.log(token.data.resetToken);

        if (token.errors?.length) {
            yield put(ActionError.failureLoad(token.errors));
            yield put(ActionLoad.hideLoad());
            return;
        }

        /*jwt: AccessToken 쿠키 새로 업데이트*/
        yield call(Commons.removeCookies, Config.BEARERTOKEN);
        yield call(Commons.createAccessToken, token.data.resetToken);

        /*완료후 메인페이지로!*/
        yield put(ActionLoad.hideLoad());
        yield call(Router.push, '/');
    } catch (error) {
        //graphQl api 500에러체크!
        yield put(ActionLoad.hideLoad());
        if (typeof window !== 'undefined') {
            yield call(Router.push, '/500');
        } else {
            //ssr에서 에러 체크후 500페이지로 보내기 위해서 에러 새로 생성!
            throw new Error(error.message);
        }
    }
}

function* DeleteUserSaga(action: ReturnType<typeof ActionLogIn.deleteUser>): Generator<any, void, any> {
    console.log('saga!:', action);

    const { data } = action.payload;

    yield put(ActionLoad.showLoad());

    try {
        //회원삭제!
        const { data: graphQlData } = yield call(Api.deleteUser, data);
        console.log('user_id!!!!!!!!!', graphQlData);

        //graphQl api 에러체크!: graphQl error array를 넘김!
        if (graphQlData.errors?.length) {
            yield put(ActionError.failureLoad(graphQlData.errors));
            yield put(ActionLoad.hideLoad());
            return;
        }

        //로그아웃 로직 적용
        yield alert(`${graphQlData.data.deleteUser.user_id} 회원 탈퇴!`);
        yield put(ActionLogIn.linkLogOut());
        yield put(ActionLoad.hideLoad());
    } catch (error) {
        //graphQl api 500에러체크!
        yield put(ActionLoad.hideLoad());
        if (typeof window !== 'undefined') {
            yield call(Router.push, '/500');
        } else {
            //ssr에서 에러 체크후 500페이지로 보내기 위해서 에러 새로 생성!
            throw new Error(error.message);
        }
    }
}

export function* watchUpdateUser() {
    yield takeEvery(ActionLogIn.UPDATE_USER, UpdateUserSaga);
}

export function* watchDeleteUser() {
    yield takeEvery(ActionLogIn.DELETE_USER, DeleteUserSaga);
}
