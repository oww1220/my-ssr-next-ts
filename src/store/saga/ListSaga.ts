import { takeEvery, put, call, delay, select } from 'redux-saga/effects';
import * as ActionTestList from '@src/store/reducer/GetTestListReducer';
import * as ActionLoad from '@src/store/reducer/LoadReducer';
import * as ActionError from '@src/store/reducer/ErrorReducer';
import * as Api from '@src/lib/Api';
import Router from 'next/router';
import { selectLogInStateData, selectBearerToken } from '@src/store/hooks/UseLogIn';

function* FetchTestListSaga(action: ReturnType<typeof ActionTestList.fetchTestList>): Generator<any, void, any> {
    console.log('saga!:', action);

    yield put(ActionLoad.showLoad());

    //yield delay(1000);
    try {
        const { data: graphQlData } = yield call(Api.getTestList);
        //console.log('allUser!', graphQlData.data.allUser);

        //graphQl api 에러체크!: graphQl error array를 넘김!
        if (graphQlData.errors?.length) {
            yield put(ActionError.failureLoad(graphQlData.errors));
            yield put(ActionLoad.hideLoad());
            return;
        }

        //graphQl api 정상동작!
        yield put(ActionTestList.fetchTestSuccess(graphQlData.data.allUser));
        yield put(ActionLoad.hideLoad());
    } catch (error) {
        //graphQl api 500에러체크!
        yield put(ActionLoad.hideLoad());
        if (typeof window !== 'undefined') {
            yield call(Router.push, '/500');
        } else {
            //ssr에서 에러 체크후 500페이지로 보내기 위해서 에러 새로 생성!
            throw new Error(error.message);
        }
    }
}

function* FetchAuthListSaga(action: ReturnType<typeof ActionTestList.fetchAuthList>): Generator<any, void, any> {
    console.log('saga!:', action);

    const logInStateData = yield select(selectLogInStateData);
    //console.log('logInStateData', logInStateData);
    //리덕스 스토어에 로그인 정보 없으면 함수종료!!
    if (!logInStateData) {
        yield put(ActionLoad.hideLoad());
        return;
    }

    /*리덕스에 저장된 토큰을 api호출 함수에 넘겨줌*/
    const bearerToken = yield select(selectBearerToken);
    //console.log('bearerToken!!!', bearerToken);

    yield put(ActionLoad.showLoad());
    try {
        //graphQl api 호출!
        const { data: graphQlData } = yield call(Api.getAuthList, logInStateData.user_id, bearerToken);

        //graphQl api 에러체크!: graphQl error array를 넘김!
        if (graphQlData.errors?.length) {
            yield put(ActionError.failureLoad(graphQlData.errors));
            yield put(ActionLoad.hideLoad());
            return;
        }

        //graphQl api 정상동작!
        console.log('authUser!', graphQlData.data.authUser);
        yield put(ActionTestList.fetchAuthSuccess(graphQlData.data.authUser));
        yield put(ActionLoad.hideLoad());
    } catch (error) {
        //graphQl api 500에러체크!
        yield put(ActionLoad.hideLoad());
        if (typeof window !== 'undefined') {
            //console.log('csr error!!!!', error.response);
            yield call(Router.push, '/500');
            //yield put(ActionError.failureLoad(error.response));
        } else {
            //console.log('ssr error!!!!', error.message);
            //ssr에서 에러 체크후 500페이지로 보내기 위해서 에러 새로 생성!
            throw new Error(error.message);
        }
    }
}

export function* watchFetchTestList() {
    yield takeEvery(ActionTestList.FETCH_TESTLIST, FetchTestListSaga);
}

export function* watchFetchAuthList() {
    yield takeEvery(ActionTestList.FETCH_AUTHLIST, FetchAuthListSaga);
}
