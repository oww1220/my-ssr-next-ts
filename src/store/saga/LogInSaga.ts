import { takeEvery, put, call, select, delay } from 'redux-saga/effects';
import * as ActionLogIn from '@src/store/reducer/LogInReducer';
import * as ActionLoad from '@src/store/reducer/LoadReducer';
import * as ActionError from '@src/store/reducer/ErrorReducer';
import * as ActionTestList from '@src/store/reducer/GetTestListReducer';
import * as Api from '@src/lib/Api';
import * as Commons from '@src/lib/Commons';
import Config from '@src/lib/Config';
import Router from 'next/router';

function* LinkLogInSaga(action: ReturnType<typeof ActionLogIn.linkLogIn>): Generator<any, void, any> {
    console.log('saga!:', action);

    const { data } = action.payload;

    yield put(ActionLoad.showLoad());

    //yield delay(500);
    try {
        //graphQl api 호출!
        const { data: graphQlData } = yield call(Api.getUser, data);

        //graphQl api 에러체크!: graphQl error array를 넘김!
        if (graphQlData.errors?.length) {
            //console.log(graphQlData);
            yield put(ActionError.failureLoad(graphQlData.errors));
            yield put(ActionLoad.hideLoad());
            return;
        }

        /*로그인 정보 및 jwt: 리덕스 스토어에 저장*/
        yield put(ActionLogIn.linkLogInSuccess(graphQlData.data.getUser.foundUser, graphQlData.data.getUser.token));

        /*jwt: AccessToken 쿠키에 저장*/
        yield call(Commons.createAccessToken, graphQlData.data.getUser.token);

        //링크 이동시키기전에 액션들 종료시키야됨!!!
        yield put(ActionLoad.hideLoad());
        //yield call(Router.push, '/');
    } catch (error) {
        //graphQl api 500에러체크!
        yield put(ActionLoad.hideLoad());
        if (typeof window !== 'undefined') {
            yield call(Router.push, '/500');
        } else {
            throw new Error(error.message);
        }
    }
}

function* LinkLogOutSaga(action: ReturnType<typeof ActionLogIn.linkLogOut>): Generator<any, void, any> {
    console.log('saga!:', action);

    yield put(ActionLoad.showLoad());
    yield put(ActionLogIn.linkLogOutSuccess());

    /*각종 로그인시 의존했던 데이터들 reset액션으로 삭세시킴*/
    //start.........

    yield put(ActionTestList.authListReset());

    //.........end

    /*jwt 쿠키에서 삭제*/
    yield call(Commons.removeCookies, Config.BEARERTOKEN);

    try {
        /*httpOnly 쿠키 서버에서 삭제*/
        const { data: graphQlData } = yield call(Api.logoutUser);

        //graphQl api 에러체크!: graphQl error array를 넘김!
        if (graphQlData.errors?.length) {
            yield put(ActionError.failureLoad(graphQlData.errors));
            yield put(ActionLoad.hideLoad());
            return;
        }

        //링크 이동시키기전에 액션들 종료시키야됨!!!
        yield put(ActionLoad.hideLoad());
        //yield call(Router.push, '/');
    } catch (error) {
        //graphQl api 500에러체크!
        yield put(ActionLoad.hideLoad());
        if (typeof window !== 'undefined') {
            yield call(Router.push, '/500');
        } else {
            //ssr에서 에러 체크후 500페이지로 보내기 위해서 에러 새로 생성!
            throw new Error(error.message);
        }
    }
}

function* UpdateRefreshTokenSaga(action: ReturnType<typeof ActionLogIn.updateRefreshToken>): Generator<any, void, any> {
    console.log('saga!:', action);
    yield put(ActionLoad.showLoad());
    try {
        /*httpOnly 쿠키 다시 받아옴 && refreshToken 재발행 && accessToken값 리턴*/
        const { data: graphQlData } = yield call(Api.refreshRefreshToken);
        //console.log(graphQlData.data.refreshRefreshToken);

        //graphQl api 에러체크!: graphQl error array를 넘김!
        if (graphQlData.errors?.length) {
            yield put(ActionError.failureLoad(graphQlData.errors));
            yield put(ActionLoad.hideLoad());
            return;
        }

        /*jwt: AccessToken 쿠키에 저장*/
        yield call(Commons.createAccessToken, graphQlData.data.refreshRefreshToken);

        //링크 이동시키기전에 액션들 종료시키야됨!!!
        yield put(ActionLoad.hideLoad());
        //yield call(Router.push, '/');
    } catch (error) {
        //graphQl api 500에러체크!
        yield put(ActionLoad.hideLoad());
        if (typeof window !== 'undefined') {
            yield call(Router.push, '/500');
        } else {
            //ssr에서 에러 체크후 500페이지로 보내기 위해서 에러 새로 생성!
            throw new Error(error.message);
        }
    }
}

export function* watchLinkLogIn() {
    yield takeEvery(ActionLogIn.LINK_LOGIN, LinkLogInSaga);
}

export function* watchLinkLogOut() {
    yield takeEvery(ActionLogIn.LINK_LOGOUT, LinkLogOutSaga);
}

export function* watchUpdateRefreshToken() {
    yield takeEvery(ActionLogIn.UPDATE_REFRESHTOKEN, UpdateRefreshTokenSaga);
}
