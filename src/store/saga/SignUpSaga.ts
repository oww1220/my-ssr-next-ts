import { takeEvery, put, call, select, delay } from 'redux-saga/effects';
import * as ActionLogIn from '@src/store/reducer/LogInReducer';
import * as ActionLoad from '@src/store/reducer/LoadReducer';
import * as ActionError from '@src/store/reducer/ErrorReducer';
import * as Api from '@src/lib/Api';
import Router from 'next/router';

function* LinkSignUpSaga(action: ReturnType<typeof ActionLogIn.linkSignUp>): Generator<any, void, any> {
    console.log('saga!:', action);

    const { data } = action.payload;

    yield put(ActionLoad.showLoad());

    //yield delay(500);

    try {
        const { data: graphQlData } = yield call(Api.createUser, data);
        console.log(graphQlData);
        //graphQl api 에러체크!: graphQl error array를 넘김!
        if (graphQlData.errors?.length) {
            yield put(ActionError.failureLoad(graphQlData.errors));
            yield put(ActionLoad.hideLoad());
            return;
        }

        yield put(ActionLoad.hideLoad());
        yield call(Router.push, '/LogIn');
    } catch (error) {
        //graphQl api 500에러체크!
        yield put(ActionLoad.hideLoad());
        if (typeof window !== 'undefined') {
            yield call(Router.push, '/500');
        } else {
            //ssr에서 에러 체크후 500페이지로 보내기 위해서 에러 새로 생성!
            throw new Error(error.message);
        }
    }
}

export function* watchLinkSignUp() {
    yield takeEvery(ActionLogIn.LINK_SIGNUP, LinkSignUpSaga);
}
