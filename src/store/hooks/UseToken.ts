import React, { useState, useEffect, useCallback } from 'react';
import jwt_decode from 'jwt-decode';
import * as Interface from '@src/interfaces/Interfaces';
import * as Commons from '@src//lib/Commons';
import useLogIn from '@src/store/hooks/UseLogIn';

const UseToken = (refreshTokenFlag: boolean, refreshTokenUpdateFlag: boolean) => {
    const { logInState, linkLogInSuccess, linkLogOutSuccess, linkLogOut, updateRefreshToken } = useLogIn();
    const [token, setToken] = useState<string | undefined>(undefined);

    useEffect(() => {
        console.log('token change:', token);
        /*새션토큰 변화만 있을시 로그인 링크 액션 호출해서 로직실행*/
        if (!logInState && token) {
            const decoded = jwt_decode<Interface.IUsersData>(token);
            //const payload: Interface.IUsersData = decoded.payload;
            console.log('%c로그아웃상태이나 쿠키에accessToken이 있으면 로그인 시킴!', 'color:violet');
            linkLogInSuccess(decoded, token);
        } else if (logInState && !token) {
            //서버에 api 호출해서 refleshToken 인증후 인증되면 acessToken 재발급 아니면 로그아웃 시킴!

            console.log('%c로그인 상태인데 쿠키에accessToken이 없으면 로그아웃 시킴!', 'color:violet');
            linkLogOutSuccess();
        }
    }, [token]);

    //refreshToken 에러시 로그아웃및 refreshToken 서버에서 삭제
    useEffect(() => {
        console.log('%c refreshToken error?', Commons.consoleStyle01, refreshTokenFlag);
        //refreshTokken 에러시 클라이언트에서 쿠키삭제요청 api호출!
        if (refreshTokenFlag) {
            console.log('%crefreshToken 인증에러시 로그아웃 및 refreshToken삭제!', 'color:violet');
            linkLogOut();
        }
    }, [refreshTokenFlag]);

    useEffect(() => {
        console.log('%c refreshToken update?', Commons.consoleStyle01, refreshTokenUpdateFlag);
        //refreshTokenUpdateFlag 만료 24시간전 클라이언트에서 refreshTokenUpdate api호출!
        if (refreshTokenUpdateFlag) {
            console.log('%crefreshToken update시 refreshTokenUpdate api호출!', 'color:violet');
            updateRefreshToken();
        }
    }, [refreshTokenUpdateFlag]);

    return {
        token,
        setToken,
    };
};

export default UseToken;
