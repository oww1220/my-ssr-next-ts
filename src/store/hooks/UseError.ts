import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { State } from '@src/store/reducer/RootReducer';

import { failureLoad } from '@src/store/reducer/ErrorReducer';

export const selectErrorData = (state: State) => state.ErrorReducer.errorData;
export const selectErrorCallBack = (state: State) => state.ErrorReducer.errorCallBack;

const UseLoad = () => {
    const dispatch = useDispatch();

    return {
        errorData: useSelector(selectErrorData),
        errorCallBack: useSelector(selectErrorCallBack),
        failureLoad: useCallback(bindActionCreators(failureLoad, dispatch), [dispatch]),
    };
};

export default UseLoad;
