import { createStore, applyMiddleware, Store } from 'redux';
import createSagaMiddleware, { END, SagaMiddleware, Task } from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createWrapper } from 'next-redux-wrapper';
import rootReducer, { State } from '@src/store/reducer/RootReducer';
import rootSaga from '@src/store/saga/RootSaga';

declare module 'redux' {
    interface Store {
        sagaTask: Task;
        close: () => END;
    }
}

const configureStore = (preloadedState?) => {
    //console.log('preloadedState!!', preloadedState);
    const sagaMiddleware = createSagaMiddleware();

    const store: Store = createStore(rootReducer, preloadedState, composeWithDevTools(applyMiddleware(sagaMiddleware)));
    store.sagaTask = sagaMiddleware.run(rootSaga);
    store.close = () => store.dispatch(END);
    return store;
};

const Wrapper = createWrapper(configureStore, { debug: false });

export default Wrapper;
