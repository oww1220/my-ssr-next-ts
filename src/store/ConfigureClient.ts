import { useMemo } from 'react';
import { ApolloClient, HttpLink, InMemoryCache, NormalizedCacheObject } from '@apollo/client';
import Config from '@src/lib/Config';

let apolloClient: ApolloClient<NormalizedCacheObject> = null;

const ConfigureClient = () => {
    const baseUrl = Config.BASEURL;
    const client = new ApolloClient({
        ssrMode: typeof window === 'undefined',
        cache: new InMemoryCache(),
        link: new HttpLink({
            uri: `${baseUrl}/graphql`, // 서버 URL (상대 주소가 아닌 절대 주소를 써야한다.)
            credentials: 'include', // `credentials`나 `headers`같은 추가적 fetch() 옵션
        }),
    });

    return client;
};

export const initializeApollo = (initialState = null) => {
    const _apolloClient = apolloClient ?? ConfigureClient();
    //console.log('%c!!!!!!!!initialState out!', 'color: #a4f644; background-color: #000;', initialState);

    //캐쉬정보확인
    const existingCache = _apolloClient.extract();
    //console.log('%c existingCache', 'color: red; background-color: #000;', existingCache);

    // 기본상태가 들어오면 캐쉬 매모리 업데이트!!
    if (initialState) {
        _apolloClient.cache.restore(initialState);
    }

    // SSG(Server Side Generation)와 SSR(Server Side Rendering)은 항상 새로운 Apollo Client를 생성한다.
    if (typeof window === 'undefined') return _apolloClient;

    // 클라이언트의 Apollo Client는 한 번만 생성한다.
    if (!apolloClient) apolloClient = _apolloClient;

    return _apolloClient;
};

export function useApollo(initialState) {
    const store = useMemo(() => initializeApollo(initialState), [initialState]);
    return store;
}
