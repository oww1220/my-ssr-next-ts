import React from 'react';
import useLoad from '@src/store/hooks/UseLoad';
import Loading from '@src/components/Loading/Loading';

interface ILoadingProviderProps {
    children: JSX.Element;
}

const LoadingProvider = ({ children }: ILoadingProviderProps) => {
    const { loadState } = useLoad();

    console.log('call:', LoadingProvider.name, '!! 로딩바 상태는?', loadState);

    return (
        <>
            {children}
            {loadState && <Loading />}
        </>
    );
};

export default LoadingProvider;
