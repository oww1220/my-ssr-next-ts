import { GetServerSideProps, GetStaticProps } from 'next/types';
import { useRouter } from 'next/router';
import { useCallback, useEffect } from 'react';
import useLogIn from '@src/store/hooks/UseLogIn';
import Login from '@src/pages/LogIn/index';
import useError from '@src/store/hooks/UseError';
import * as Commons from '@src/lib/Commons';

interface IWithAuthProps<T> {
    (props: T): JSX.Element;
    getServerSideProps?: GetServerSideProps;
    getStaticProps?: GetStaticProps;
}

const WithAuth = <T extends {}>(Component: IWithAuthProps<T>) => {
    const Auth = (props: T) => {
        //console.log('call:', Auth.name, '!!');
        const router = useRouter();
        const { logInState } = useLogIn();
        const { failureLoad } = useError();

        const alertRedirect = useCallback(() => {
            const error: any = [
                {
                    message: '로그인 필요!',
                    extensions: {
                        code: '',
                    },
                },
            ];
            failureLoad(error);
            router.push('/LogIn');
        }, []);

        // __NEXT_REDUX_WRAPPER_HYDRATE__액션이 더 늦게 실행이되서 타임아웃 함수를;;;
        // getServerSideProps 호출로 인한 ssr로 서버에서 data 받는 __NEXT_REDUX_WRAPPER_HYDRATE__액션은 테스트 결과 csr useEffect이후에 실행되는...;;
        // 즉 한페이지에 csr, ssr 혼용시키면 순서보장이 안되는 단점이 있는듯함.
        useEffect(() => {
            const getToken = Commons.getBearerToken();
            if (!getToken) {
                setTimeout(() => {
                    alertRedirect();
                }, 1000);
            }
        }, []);

        // If user is not logged in, return login component
        if (!logInState) {
            return (
                <>
                    <Login />
                </>
            );
        }

        return <Component {...props} />;
    };

    // Copy getInitial props so it will run as well
    if (Component.getServerSideProps) {
        Auth.getServerSideProps = Component.getServerSideProps;
    }

    return Auth;
};

export default WithAuth;
