import { useRouter } from 'next/router';
import { useEffect } from 'react';
import jwt_decode from 'jwt-decode';
import useToken from '@src/store/hooks/UseToken';
import * as Commons from '@src//lib/Commons';
import * as Interface from '@src/interfaces/Interfaces';
import Config from 'src/lib/Config';

interface ILoginCheckProviderProps {
    children: JSX.Element;
    refreshTokenFlag: boolean;
    refreshTokenUpdateFlag: boolean;
}
const LoginCheckProvider = ({ children, refreshTokenFlag, refreshTokenUpdateFlag }: ILoginCheckProviderProps) => {
    console.log('call:', LoginCheckProvider.name, '!!');

    const router = useRouter();
    const { setToken } = useToken(refreshTokenFlag, refreshTokenUpdateFlag);

    //url 변경시마다 토큰체크해서 토큰 setter (토큰유무에 따라 로그인 여부결정함)~~
    useEffect(() => {
        const getToken = Commons.getBearerToken();
        setToken(getToken);

        //gitlab pages redirect use
        //if (Config.APP_NAME && Commons.regExp(location.pathname) === Commons.regExp(Config.APP_NAME)) {
        //console.log('APP_NAME', Config.APP_NAME, Commons.regExp(location.pathname));
        //history.push('/');
        //}
    }, [router.pathname]);

    return <>{children}</>;
};

export default LoginCheckProvider;
