import Head from 'next/head';
import { useEffect, useCallback } from 'react';
import { useForm } from 'react-hook-form';
import useLogIn from '@src/store/hooks/UseLogIn';
import useError from '@src/store/hooks/UseError';
import WithAuth from '@src/Provider/WithAuth';

const DeleteUser = () => {
    const { logInState, logInStateData, deleteUser } = useLogIn();
    const { failureLoad } = useError();

    const onSubmit = useCallback(() => {
        if (logInStateData) {
            const error: any = [
                {
                    message: '정말 탈퇴하시겠습니까?',
                    extensions: {
                        code: '',
                    },
                },
            ];
            const errorCallBack = () => {
                deleteUser(logInStateData);
            };
            /*
            const error = {
                data: errorData,
                errorCallBack: () => {
                    deleteUser(logInStateData);
                },
            };*/
            failureLoad(error, errorCallBack);
            //deleteUser(logInStateData);
        }
    }, [logInStateData]); //의존성 배열에 빈 값을 넣으면 새로고침시 로그인 체크 할 경우 함수 클로져환경이 안바뀜(로그인 체크 로직이 effect안에 들어있어서..)..그래서 logInStateData 이 null로 유지됨

    return (
        <>
            <Head>
                <title>DeleteUser------</title>
            </Head>
            <div className="App-header">
                <div className="LogIn">
                    <h1>회원탈퇴</h1>
                    {logInStateData && (
                        <button type="button" className="btn" onClick={onSubmit}>
                            회원탈퇴
                        </button>
                    )}
                </div>
            </div>
        </>
    );
};

export default WithAuth(DeleteUser);
