import { useEffect } from 'react';
import { AppProps, AppContext } from 'next/app';
import Layout from '@src/components/Layout/Layout';
import * as cookie from 'cookie';
import jwt_decode from 'jwt-decode';
import * as Interface from '@src/interfaces/Interfaces';
import * as Commons from '@src/lib/Commons';
import * as Api from '@src/lib/Api';
import { linkLogInSuccess } from '@src/store/reducer/LogInReducer';
import { linkLogOutSuccess } from '@src/store/reducer/LogInReducer';
import { failureLoad } from '@src/store/reducer/ErrorReducer';

//redux
import Wrapper from '@src/store/ConfigureStore';

//apollo
import { ApolloProvider } from '@apollo/client';
import { useApollo } from '@src/store/ConfigureClient';

import useLogIn from '@src/store/hooks/UseLogIn';
import '@src/assets/scss/Root.scss';

//page폴더에 있는 컴퍼넌트들은 링크이동할시마다 무저건 실행됨
const App = ({ Component, pageProps, router }: AppProps) => {
    //루트 시작 구분 console !!
    console.log(
        `%c-------------------------------- (${Component.name} : ${router.pathname}) --------------------------------`,
        Commons.consoleStyle00,
    );
    //console.log(router);

    const { initialApolloState, refreshTokenFlag, refreshAccessToken, refreshTokenUpdateFlag } = pageProps;
    //console.log('%c initialApolloState ', Commons.consoleStyle01, pageProps?.initialApolloState);
    const apolloClient = useApollo(initialApolloState);

    //넥스트 웹서버에서 받은 refreshAccessToken 있으면 쿠키를 바로 구움
    if (refreshAccessToken) {
        console.log('%caccessToken 재 발행!!!!쿠키를 다시구움!', 'color:violet');
        Commons.createAccessToken(refreshAccessToken);
    }

    //test해본 결과, useSelector 및 useDispatch로 만들어진,, state 값및 action생성자 함수를 참조하는 컴퍼넌트들은 구독대상임!!
    //즉 로그인 상태가 바뀌거나 그러면 _app root 무조건 호출;;;; 그래서 로직을 이동시킴;;
    //const { linkLogOut } = useLogIn();

    //useEffect(() => {
    //console.log('%c refreshToken error?', Commons.consoleStyle01, refreshTokenFlag);
    //refreshTokken 에러시 클라이언트에서 쿠키삭제요청 api호출!
    //if (refreshTokenFlag) {
    //linkLogOut();
    //}
    //}, [refreshTokenFlag]);

    return (
        <ApolloProvider client={apolloClient}>
            <Layout refreshTokenFlag={refreshTokenFlag} refreshTokenUpdateFlag={refreshTokenUpdateFlag}>
                <Component {...pageProps} />
            </Layout>
        </ApolloProvider>
    );
};

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
// static site를 만들기위해, 또는 자동 정적 최적화( getServerSideProps및 getInitialProps 유무에 따라 판별)를 위해 아래 getInitialProps 메소드는 삭제시키야된다! 그럼 공통 서버 로직은 어디에다 넣지???;;; 페이지 이동시나 새로고침시 refreshToken체크를 해야되니.;;; 빌드시 자동정적페이지 생성 기능을 쓰고는 싶으나..;; 로그인체크 로직을 위해서 모든 페이지 라우팅에 체크를 해야되니....;;;;

//getInitialProps 는 서버, 클라이언트 양쪽다 실행됨.

//test결과 getInitialProps(Wrapper.withRedux로 리턴된 컴퍼넌트임), getServerSideProps(Wrapper.getServerSideProps로 리턴된 컴퍼넌트임) 당 __NEXT_REDUX_WRAPPER_HYDRATE__ action이 dispatch됨;;
// 모든 page 컴퍼넌트 ssr, csr시 __NEXT_REDUX_WRAPPER_HYDRATE__ action 1번 dispatch됨
// getServerSideProps(Wrapper.getServerSideProps로 리턴된 컴퍼넌트임) 있는 page 컴퍼넌트는 ssr, csr시 __NEXT_REDUX_WRAPPER_HYDRATE__ action 2번 dispatch됨, 순수getServerSideProps는 해당안됨
App.getInitialProps = async (context: AppContext) => {
    const { store, req, res, pathname } = context.ctx;
    let refreshTokenUpdateFlag = false;
    let refreshTokenFlag = false;
    let refreshAccessToken = '';
    //서버사이드 로그인체크
    //****************************************
    // ssr(window.location.reload): getServerSideProps 존재하는 컴퍼넌트=>getInitialProps(서버, 브라우저), getServerSideProp(서버)호출, getServerSideProps 없는 컴퍼넌트=>getInitialProps(서버, 브라우저)
    // csr(라우팅 이동): getServerSideProps 존재하는 컴퍼넌트=>getInitialProps(서버, 브라우저), getServerSideProp(서버)호출, getServerSideProps 없는 컴퍼넌트=>둘다 호출안됨 즉 기존방식대로 동작!!!
    // ***ssr(window.location.reload) 시에는 서버에서 로직실행 후 상태값을 문서에 넣어서 text/html 업데이트,
    // ***csr(라우팅 이동) 시에는 서버에서 로직실행 후 application/json 으로 상태값만 넘김
    //****************************************
    //Next는 페이지에 getInitialProps, getServerSideProp가 없으면 해당 페이지는 static페이지라고 판단하여 pre-render한다.
    //페이지를 pre-render한다는 말은 빌드할떄 그 페이지를 html파일로 만들어버린다는 얘기이다.
    //유저의 요청이 들어왔을때 서버에서 html파일을 만들어서 주는게 아니라 미리 빌드할때 페이지를 만들어둔다.
    //그 페이지에서 비동기 요청을 해서 받아온 데이터로 마크업이 달라지지 않기 때문에 미리 빌드타임에 pre-render하는것이다.
    if (req) console.log('------------getInitialProps------------');
    if (req?.headers.cookie) {
        const cookies = cookie.parse(req.headers.cookie);
        console.log(cookies);
        try {
            //accessToken이 없고 refreshToken만 있어도 (로그인시키야됨)
            if (cookies.refreshToken && !cookies.BearerToken) {
                console.log('서버 로그인체크 refreshToken!!!!!!', cookies);

                //graphQl api 호출! -- accessToken 재발급!
                const { data: graphQlData } = await Api.refreshAccessToken(cookies.refreshToken);
                console.log('refreshAccessToken!!!!', graphQlData);

                if (graphQlData.data.refreshAccessToken === 'update') {
                    //브라우져에서 refreshToken 쿠키를 업데이트 해야되니 제어권을 클라이언트에 넘김!!
                    refreshTokenUpdateFlag = true;
                    return {
                        pageProps: {
                            refreshTokenUpdateFlag,
                            refreshTokenFlag,
                            refreshAccessToken,
                        },
                    };
                }

                //graphQl api 에러체크!: graphQl error array를 넘김!
                if (graphQlData.errors?.length) {
                    //error 생성!
                    store.dispatch(failureLoad(graphQlData.errors));

                    //refreshToken 에러시에는 무조건쿠키 삭제를!
                    //브라우져에서 쿠키를 삭제해야되니 제어권을 클라이언트에 넘김!!
                    refreshTokenFlag = true;
                    return {
                        pageProps: {
                            refreshTokenUpdateFlag,
                            refreshTokenFlag,
                            refreshAccessToken,
                        },
                    };
                }

                //jwt 디코드시켜서 로그인상태및 기본정보 업데이트 및 클라이언트에 accessToken 넘겨줌!
                refreshAccessToken = graphQlData.data.refreshAccessToken;
                const decoded = jwt_decode<Interface.IUsersData>(graphQlData.data.refreshAccessToken);
                store.dispatch(linkLogInSuccess(decoded, graphQlData.data.refreshAccessToken));
            }

            //브라우져에서 가져온 쿠키에 ..... accessToken 이 있다면 (로그인시키야됨)
            if (cookies.BearerToken) {
                console.log('서버 로그인체크 accessToken!!!!', cookies.BearerToken);

                //jwt 디코드시켜서 로그인상태및 기본정보 업데이트!
                const decoded = jwt_decode<Interface.IUsersData>(cookies.BearerToken);
                store.dispatch(linkLogInSuccess(decoded, cookies.BearerToken));
            }
        } catch (error) {
            //500 api 서버죽었을경우!
            console.log('%c api서버500 error', Commons.consoleStyle04, error.response, pathname);
            //이 조건을 안넣으면 무한루프 (루트에서 체크하는 로직이기 때문에)!
            if (pathname !== '/500') {
                store.dispatch(linkLogOutSuccess());
                //ssr용 리다이랙트 패턴!
                return res.writeHead(302, { Location: '/500' }).end();
            }
        }
    }
    return {
        pageProps: {
            refreshTokenUpdateFlag,
            refreshTokenFlag,
            refreshAccessToken,
        },
    };
};

//__NEXT_REDUX_WRAPPER_HYDRATE__ action이 Wrapper.getServerSideProps 호출되는 컴퍼넌트가 라우트 될때마다 서버 redux store state 주입됨
export default Wrapper.withRedux(App);
