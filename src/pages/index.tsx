import Head from 'next/head';
import Link from 'next/link';

import Config from '@src/lib/Config';
import img from '@src/assets/images/logo192.png';

export const Home = () => {
    //console.log(Config);
    return (
        <div className="home">
            <Head>
                <title>Create Next App</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <div className="App-header">
                <h1>
                    <img src={img} width={'30px'} />
                    <span>HOME Links!</span>
                </h1>
                <div className="home-nav">
                    <Link href="/TestNormal">TestNormal 로 이동</Link>
                    <Link href="/TestNormal/depth">TestNormal/depth로 이동</Link>
                    <Link href="/TestList">TestList 로 이동 (redux-saga, apollo)</Link>
                    <Link href="/TestSaga">TestSaga 로 이동 (redux-saga)</Link>
                    <Link href="/TestApollo">TestApollo 로 이동 (apollo)</Link>
                    <Link href="/AuthPage">AuthPage로 이동 (로그인시만 들어가짐)</Link>
                </div>
            </div>
        </div>
    );
};

export default Home;
