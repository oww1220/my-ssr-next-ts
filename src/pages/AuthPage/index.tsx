import Head from 'next/head';
import { useCallback, useEffect, useState } from 'react';
import useLogIn from '@src/store/hooks/UseLogIn';
import useTestList from '@src/store/hooks/UseTestList';
import { fetchAuthList } from '@src/store/reducer/GetTestListReducer';
import Wrapper from '@src/store/ConfigureStore';
import WithAuth from '@src/Provider/WithAuth';
import { linkLogOutSuccess } from '@src/store/reducer/LogInReducer';
import * as Commons from '@src/lib/Commons';

interface IAuthPageProps {
    testNumber: number;
}

export const AuthPage = (props: IAuthPageProps) => {
    //const { logInStateData, logInState } = useLogIn();
    const { authObj, fetchAuthList } = useTestList();

    console.log('props!!!!!!!!!!!!', props);

    //기존방식으로 클라이언트에서 마운트후(브라우저 페인팅후)에 실행
    //useEffect(() => {
    //if (logInStateData) fetchAuthList();
    //}, [logInStateData]);

    return (
        <>
            <Head>
                <title>AuthPage------</title>
            </Head>
            <div className="App-header">
                <h1>AuthPage------</h1>
                <div className="AuthPage">
                    로그인후 진입가능
                    <br />
                    couponCnt: {authObj && authObj.couponCnt},<br />
                    estimateSaveCnt: {authObj && authObj.estimateSaveCnt},
                </div>
            </div>
        </>
    );
};

//서버에서 실행되어져서 -> [페이지 이름].json 파일로 응답결과가 떨어짐!
export const getServerSideProps = Wrapper.getServerSideProps(async (context) => {
    //redux-saga start
    const { store, req, res } = context;
    try {
        /* action */
        store.dispatch(fetchAuthList());
    } catch (error) {
        //500 api 서버죽었을경우!
        console.log('%c api서버500 error!!!!', Commons.consoleStyle04, error);
        store.dispatch(linkLogOutSuccess());
        return res.writeHead(302, { Location: '/500' }).end();
    }

    /* observing end */
    store.close(); // redux-saga의 END 액션 이용하여 saga task가 종료되도록 한다.
    await store.sagaTask.toPromise(); // saga task가 모두 종료되면 resolve 된다.
    //redux-saga end

    //서버에서 계산해서 컴퍼넌트로 props 주입!
    return {
        props: {
            testNumber: 111111,
        },
    };
});

export default WithAuth(AuthPage);
