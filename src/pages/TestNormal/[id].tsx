import { useRouter } from 'next/router';

export const Post = () => {
    const router = useRouter();
    return (
        <>
            <div className="App-header">
                <h1>TestNormal-----Post-{router.query.id}</h1>
            </div>
        </>
    );
};

export default Post;
