const NotFound = () => {
    return (
        <div className="not-found">
            <span>404 - Page Not Found !!</span>
        </div>
    );
};

export default NotFound;
