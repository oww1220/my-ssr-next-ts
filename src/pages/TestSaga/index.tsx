import Head from 'next/head';
import { useEffect, useCallback, useState } from 'react';
import useTestList from '@src/store/hooks/UseTestList';
import { fetchTestList } from '@src/store/reducer/GetTestListReducer';
import Wrapper from '@src/store/ConfigureStore';

export const TestSaga = () => {
    console.log('call:', TestSaga.name, '!!');

    const { testLists, fetchTestList } = useTestList();

    useEffect(() => {
        console.log('!!!!effect!!!!');
        //const query = queryString.parse(location.search);
        //console.log(location, query);

        //서버에서 불러와서 api call할 필요가 없음
        //fetchTestList();
    }, []);

    return (
        <>
            <Head>
                <title>TestSaga------</title>
            </Head>
            <div className="App-header">
                <h1>TestSaga------</h1>

                <ul>
                    {testLists.map(({ age, _id }, idx: number) => (
                        <li key={_id}>{age}</li>
                    ))}
                </ul>
            </div>
        </>
    );
};

//서버실행 콜백함수!
export const getServerSideProps = Wrapper.getServerSideProps(async (context) => {
    console.log('------------getServerSideProp------------');
    //redux-saga start
    const { store } = context;
    store.dispatch(fetchTestList());
    store.close(); // redux-saga의 END 액션 이용하여 saga task가 종료되도록 한다.
    await store.sagaTask.toPromise(); // saga task가 모두 종료되면 resolve 된다.
    //redux-saga end
});

export default TestSaga;
