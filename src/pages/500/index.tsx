const ServerError = () => {
    return (
        <div className="not-found">
            <span>500 - Server-side error occurred !!</span>
        </div>
    );
};

export default ServerError;
