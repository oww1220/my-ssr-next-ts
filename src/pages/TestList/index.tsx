import Head from 'next/head';
import { useEffect, useCallback, useState } from 'react';
import * as Api from '@src/lib/Api';
import useTestList from '@src/store/hooks/UseTestList';
import { fetchTestList } from '@src/store/reducer/GetTestListReducer';
import Wrapper from '@src/store/ConfigureStore';
import * as Interface from '@src/interfaces/Interfaces';

//apollo
import { useQuery } from '@apollo/client';
import { initializeApollo } from '@src/store/ConfigureClient';

/*test 컨퍼넌트*/
const TestCountInner = () => {
    //console.log('call:', TestCountInner.name, '!!');
    return <span>현재</span>;
};

/*카우터 테스터 */
export const Counter = () => {
    console.log('call:', Counter.name, '!!');
    const [count, setCount] = useState(0);

    useEffect(() => {
        const id = setInterval(() => {
            setCount((count) => count + 1);
        }, 1000);
        return () => {
            console.log('clear');
            return clearInterval(id);
        };
    }, []);
    return (
        <h1>
            <TestCountInner />
            {count}
        </h1>
    );
};

export const TestList = () => {
    console.log('call:', TestList.name, '!!');

    const { testLists, fetchTestList } = useTestList();
    const { loading, error, data } = useQuery(Api.GET_ALLUSER);

    useEffect(() => {
        console.log('!!!!effect!!!!');
        //const query = queryString.parse(location.search);
        //console.log(location, query);

        //서버에서 불러와서 api call할 필요가 없음
        //fetchTestList();
    }, []);

    return (
        <>
            <Head>
                <title>TestList------</title>
            </Head>
            <div className="App-header">
                <h1>TestList------</h1>
                <Counter />
                <ul>
                    {testLists.map(({ age, _id }, idx: number) => (
                        <li key={_id}>{age}</li>
                    ))}
                </ul>

                <ul>
                    {data?.allUser.map(({ gender, _id }: Interface.IUsersData, idx: number) => (
                        <li key={_id}>아폴로{gender}</li>
                    ))}
                </ul>
            </div>
        </>
    );
};

//서버실행 콜백함수!
export const getServerSideProps = Wrapper.getServerSideProps(async (context) => {
    console.log('------------getServerSideProp------------');
    //redux-saga start
    const { store } = context;
    store.dispatch(fetchTestList());
    store.close(); // redux-saga의 END 액션 이용하여 saga task가 종료되도록 한다.
    await store.sagaTask.toPromise(); // saga task가 모두 종료되면 resolve 된다.
    //redux-saga end

    //apollo start -- ssr랜더링에서는 apolloClient를 생성해서 데이터를 받아서 initialApolloState에 넣어서 클라이언트에 넘겨줌
    const apolloClient = initializeApollo(null);
    await apolloClient.query({
        query: Api.GET_ALLUSER,
    });
    return {
        props: {
            initialApolloState: apolloClient.cache.extract(),
        },
    };
    //apollo end
});

/*
//apollo만 있는경우!
export const getServerSideProps = async (context) => {
    const apolloClient = initializeApollo(null);
    await apolloClient.query({
        query: Api.GET_ALLUSER,
    });
    return {
        props: {
            initialApolloState: apolloClient.cache.extract(),
        },
    }
};*/

export default TestList;
