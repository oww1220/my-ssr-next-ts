import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useCallback } from 'react';
import { useForm } from 'react-hook-form';
import useLogIn from '@src/store/hooks/UseLogIn';

const SignUp = () => {
    const { logInState, linkSignUp } = useLogIn();
    const { register, handleSubmit, watch, errors } = useForm();
    const router = useRouter();

    const onSubmit = useCallback((data) => {
        linkSignUp(data);
    }, []);

    useEffect(() => {
        if (logInState) router.push('/');
    }, [logInState]);

    return (
        <>
            <Head>
                <title>SignUp------</title>
            </Head>
            <div className="App-header">
                <div className="SignUp">
                    <h1>SignUp</h1>

                    <form>
                        <span>아이디</span>
                        <input
                            type={'text'}
                            name="user_id"
                            ref={register({ required: true })}
                            placeholder={'user_id'}
                        />
                        {errors.user_id && <span className="error-text">'user_id is required'</span>}

                        <span>이름</span>
                        <input
                            type={'text'}
                            name="user_name"
                            ref={register({ required: true })}
                            placeholder={'user_name'}
                        />
                        {errors.user_name && <span className="error-text">'user_name is required'</span>}

                        <span>성별</span>
                        <input type={'text'} name="gender" ref={register({ required: true })} placeholder={'gender'} />
                        {errors.gender && <span className="error-text">'gender is required'</span>}

                        <span>나이</span>
                        <input type={'text'} name="age" ref={register({ required: true })} placeholder={'age'} />
                        {errors.age && <span className="error-text">'age is required'</span>}

                        <span>비밀번호</span>
                        <input
                            type={'text'}
                            name="password"
                            ref={register({ required: true })}
                            placeholder={'password'}
                        />
                        {errors.password && <span className="error-text">'password is required'</span>}
                    </form>

                    <button type="button" className="btn" onClick={handleSubmit(onSubmit)}>
                        전송
                    </button>
                </div>
            </div>
        </>
    );
};

export default SignUp;
