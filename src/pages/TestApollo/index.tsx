import Head from 'next/head';
import * as Api from '@src/lib/Api';
import * as Interface from '@src/interfaces/Interfaces';

//apollo
import { useQuery } from '@apollo/client';
import { initializeApollo } from '@src/store/ConfigureClient';
import Loading from '@src/components/Loading/Loading';

export const TestApollo = () => {
    console.log('call:', TestApollo.name, '!!');

    const { loading, error, data } = useQuery(Api.GET_ALLUSER);

    if (loading) return <Loading />;

    if (error) {
        console.log(error.message);
        return <div className={'posts-error-message'}>error occured!</div>;
    }

    return (
        <>
            <Head>
                <title>TestApollo------</title>
            </Head>
            <div className="App-header">
                <h1>TestApollo------</h1>
                <ul>
                    {data?.allUser.map(({ gender, _id }: Interface.IUsersData, idx: number) => (
                        <li key={_id}>아폴로{gender}</li>
                    ))}
                </ul>
            </div>
        </>
    );
};

//apollo만 있는경우!
export const getServerSideProps = async (context) => {
    console.log('------------getServerSideProp------------');
    const apolloClient = initializeApollo(null);
    await apolloClient.query({
        query: Api.GET_ALLUSER,
    });
    return {
        props: {
            initialApolloState: apolloClient.cache.extract(),
        },
    };
};

export default TestApollo;
