import Head from 'next/head';
import { useRouter } from 'next/router';
import { useCallback, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import useLogIn from '@src/store/hooks/UseLogIn';
import GoogleBtn from '@src/components/LoginBtns/GoogleBtn';

export const LogIn = () => {
    const { linkLogIn, logInState } = useLogIn();
    const { register, handleSubmit, watch, errors } = useForm();
    const router = useRouter();

    const onSubmit = useCallback((data) => {
        linkLogIn(data);
        //console.log(data);
    }, []);

    useEffect(() => {
        if (logInState) router.push('/');
    }, [logInState]);

    return (
        <>
            <Head>
                <title>LogIn------</title>
            </Head>
            <div className="App-header">
                <div className="LogIn">
                    <h1>LogIn</h1>
                    <form>
                        <input
                            type={'text'}
                            name="user_id"
                            ref={register({ required: true })}
                            placeholder={'user_id'}
                        />
                        {errors.user_id && <span className="error-text">'user_id is required'</span>}
                        <input
                            type={'text'}
                            name="password"
                            ref={register({ required: true })}
                            placeholder={'password'}
                        />
                        {errors.password && <span className="error-text">'password is required'</span>}
                    </form>

                    <button type="button" className="btn" onClick={handleSubmit(onSubmit)}>
                        로그인
                    </button>
                    <GoogleBtn />
                </div>
            </div>
        </>
    );
};

export default LogIn;
