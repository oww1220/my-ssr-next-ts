import { useCallback, useEffect, useState } from 'react';
import { GoogleLogin, GoogleLogout } from 'react-google-login';
import Config from '@src/lib/Config';

const GoogleBtn = () => {
    const [isLogin, setIsLogin] = useState(false);

    const onLoginGoogle = (response) => {
        console.log(response);
        setIsLogin(true);
    };
    const onLoginGoogleFailure = (response) => {
        console.log('구글 로그인 실패!');
    };
    const onLogoutGoogle = () => {
        console.log(111);
        setIsLogin(false);
    };
    const onLogoutGoogleFailure = () => {
        console.log('구글 로그아웃 실패!');
    };

    return (
        <div>
            {!isLogin ? (
                <GoogleLogin
                    clientId={Config.GOOGLE_CLIENT_ID}
                    buttonText="Login"
                    onSuccess={onLoginGoogle}
                    onFailure={onLoginGoogleFailure}
                    cookiePolicy={'single_host_origin'}
                />
            ) : (
                <GoogleLogout
                    clientId={Config.GOOGLE_CLIENT_ID}
                    buttonText="Logout"
                    onLogoutSuccess={onLogoutGoogle}
                    onFailure={onLogoutGoogleFailure}
                ></GoogleLogout>
            )}
        </div>
    );
};

export default GoogleBtn;
