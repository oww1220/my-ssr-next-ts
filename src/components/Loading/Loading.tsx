import roader from '@src/assets/images/loader.svg';

const Loading = () => {
    return (
        <div id="pageloading" className="pageloading_wrap" style={{ display: 'block' }}>
            <div className="pageloading">
                <img src={roader} alt="loading" />
            </div>
        </div>
    );
};

export default Loading;
