import { useRouter } from 'next/router';
import { useEffect, useCallback } from 'react';
import Link from 'next/link';
import useLogIn from '@src/store/hooks/UseLogIn';

const Header = () => {
    //console.log('call:', Header.name, '!!');

    const { logInState, logInStateData, linkLogOut } = useLogIn();
    const router = useRouter();

    const historyBack = useCallback(() => {
        router.back();
    }, []);

    const historyHome = useCallback(() => {
        router.push('/');
    }, []);

    const LinkLogOut = useCallback(() => {
        linkLogOut();
    }, []);

    /*
	useEffect(() => {
		console.log(history, router.pathname);
	});*/

    return (
        <>
            <div className="Header">
                <div className="left">
                    {router.pathname === '/' ? (
                        ''
                    ) : (
                        <>
                            <button type="button" onClick={historyBack}>
                                이전으로
                            </button>
                            <button type="button" onClick={historyHome}>
                                홈으로
                            </button>
                        </>
                    )}
                </div>
                <div className="right">
                    {logInState ? (
                        <div>
                            <span>{logInStateData && logInStateData.user_id} 님</span>
                            <Link href="/UpdateUser">회원정보 수정</Link>
                            <Link href="/DeleteUser">회원탈퇴</Link>
                            <button type="button" onClick={LinkLogOut}>
                                로그아웃 하기
                            </button>
                        </div>
                    ) : (
                        <>
                            <Link href="/SignUp">SignUp 로</Link>
                            <Link href="/LogIn">LogIn 로</Link>
                        </>
                    )}
                </div>
            </div>
        </>
    );
};

export default Header;
