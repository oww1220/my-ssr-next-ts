import React, { useEffect } from 'react';
import LoadingProvider from '@src/Provider/LoadingProvider';
import Header from '@src/components/Layout/Header';
import Footer from '@src/components/Layout/Footer';
import ErrorProvider from '@src/Provider/ErrorProvider';
import LoginCheckProvider from '@src/Provider/LoginCheckProvider';
import * as Commons from '@src/lib/Commons';

interface ILayoutProps {
    children: JSX.Element;
    refreshTokenFlag: boolean;
    refreshTokenUpdateFlag: boolean;
}

const Layout = ({ children, refreshTokenFlag, refreshTokenUpdateFlag }: ILayoutProps) => {
    //console.log('%c children', 'color:blue;', children);

    //React.createElement(component, props, ...children)
    //FunctionComponent
    //function createElement<T>(type: React.FunctionComponent<T>, props?: React.Attributes & T, ...children: React.ReactNode[]): React.FunctionComponentElement
    //HTMLElement
    //function createElement<{className: string;}, HTMLElement>(type: keyof React.ReactHTML, props?: React.ClassAttributes<HTMLElement> & {className: string;}, ...children: React.ReactNode[]): React.DetailedReactHTMLElement
    const element = React.createElement(
        LoadingProvider,
        null,
        React.createElement('div', { className: 'app' }, children),
    );

    useEffect(() => {
        //rem 계산
        Commons.remCaculate();
        window.addEventListener('resize', Commons.remCaculate);
        return () => {
            //console.log('clear!!!!!!!!!');
        };
    }, []);

    return (
        <LoadingProvider>
            <ErrorProvider>
                <LoginCheckProvider refreshTokenFlag={refreshTokenFlag} refreshTokenUpdateFlag={refreshTokenUpdateFlag}>
                    <>
                        <Header />
                        {/* children 을 파라디터로 주입하는 방법!}<div className="App" children={children}/>{*/}
                        <div className="App">
                            {/* 이미 React.creatElement로 실행해서 return 된 객체를 넣어줌! 즉 render update대상이 아님! --- 유추하건데 React.createElement로 실행된 컴퍼넌트들만 추후에 실행되는듯 위 element 변수 예제!!*/}
                            {children}
                        </div>
                        <Footer />
                    </>
                </LoginCheckProvider>
            </ErrorProvider>
        </LoadingProvider>
    );
};

export default Layout;
