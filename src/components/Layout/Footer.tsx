import Config from '@src/lib/Config';

const Footer = () => {
    return (
        <>
            <div className="footer">
                <span className="app-env">env : {Config.APP_ENV}</span>
                <span className="app-version">version : {Config.APP_VERSION}</span>
            </div>
        </>
    );
};

export default Footer;
