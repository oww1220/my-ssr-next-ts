import React from 'react';
import { call, put, select } from 'redux-saga/effects';
import * as matchers from 'redux-saga-test-plan/matchers';
import { expectSaga } from 'redux-saga-test-plan';
import { throwError } from 'redux-saga-test-plan/providers';
import Router from 'next/router';
import Config from '@src/lib/Config';
import * as Commons from '@src/lib/Commons';
import * as Const from '@src/__test__/Mock/Const';
import * as Api from '@src/lib/Api';
import * as ActionLogIn from '@src/store/reducer/LogInReducer';
import * as ActionLoad from '@src/store/reducer/LoadReducer';
import * as ActionError from '@src/store/reducer/ErrorReducer';
import { watchUpdateUser, watchDeleteUser } from '@src/store/saga/UpdateSaga';

//mocking 함수
window.alert = jest.fn();
Router.push = jest.fn();

describe('watchUpdateUser', () => {
    it('saga!:UpdateUser success!!', () => {
        const date = new Date();
        date.setTime(date.getTime() + 1 * 60 * 1000);
        return expectSaga(watchUpdateUser)
            .dispatch(ActionLogIn.updateUser(Const.Fake_logInStateData))
            .provide([
                [call(Api.updateUser, Const.Fake_logInStateData), Const.Fake_updateUser], //DB update!
                [call(Api.resetToken, Const.Fake_logInStateData), Const.Fake_bearerToken], //reset bearertoken!
                [call(Commons.removeCookies, Config.BEARERTOKEN), {}], //쿠키삭제
                [call(Commons.setCookies, Config.BEARERTOKEN, Const.Fake_bearerToken, { expires: date }), {}], //쿠키설정
            ])
            .put(ActionLoad.showLoad())
            .put(ActionLogIn.updateUserSuccess(Const.Fake_updateUser.data.data.updateUser)) //redux store update!
            .put(ActionLoad.hideLoad())
            .silentRun();
    });

    it('saga!:UpdateUser failure!!', () => {
        //const error = throwError(new Error('UpdateUser error!!'));
        return expectSaga(watchUpdateUser)
            .dispatch(ActionLogIn.updateUser(Const.Fake_logInStateData))
            .provide([[call(Api.updateUser, Const.Fake_logInStateData), Const.Fake_Error]])
            .put(ActionLoad.showLoad())
            .put(ActionError.failureLoad(Const.Fake_Error.data.errors))
            .put(ActionLoad.hideLoad())
            .silentRun();
    });
});

describe('watchDeleteUser', () => {
    it('saga!:DeleteUser success!!', () => {
        return expectSaga(watchDeleteUser)
            .dispatch(ActionLogIn.deleteUser(Const.Fake_logInStateData))
            .provide([
                [call(Api.deleteUser, Const.Fake_logInStateData), Const.Fake_deleteUser], //DB delete!
                //[call(window.alert, `${Const.Fake_logInStateData.user_id} 회원 탈퇴!`), undefined],
            ])
            .put(ActionLoad.showLoad())
            .put(ActionLogIn.linkLogOut()) //DB delete and logout!
            .put(ActionLoad.hideLoad())
            .silentRun();
    });

    it('saga!:DeleteUser failure!!', () => {
        //const error = throwError(new Error('DeleteUser error!!'));
        return expectSaga(watchDeleteUser)
            .dispatch(ActionLogIn.deleteUser(Const.Fake_logInStateData))
            .provide([[call(Api.deleteUser, Const.Fake_logInStateData), Const.Fake_Error]])
            .put(ActionLoad.showLoad())
            .put(ActionError.failureLoad(Const.Fake_Error.data.errors))
            .put(ActionLoad.hideLoad())
            .silentRun();
    });
});
