import React from 'react';
import { render, screen } from '@testing-library/react';

describe('test!!', () => {
    test('it should filter by a search term (link)', () => {
        const input = [{ id: 3, url: 'https://www.link3.dev' }];

        const output = [{ id: 3, url: 'https://www.link3.dev' }];

        expect(input).toEqual(output);
    });
});
