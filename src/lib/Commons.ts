import Cookies, { CookieAttributes } from 'js-cookie';
import Config from '@src/lib/Config';

/*console.style*/
export const consoleStyle00 = ['font-weight: bold', 'color: green'].join(';');
export const consoleStyle01 = ['font-weight: bold', 'color: #a4f644', 'background-color: #000'].join(';');
export const consoleStyle02 = ['font-weight: bold', 'color: red', 'background-color: #000'].join(';');
export const consoleStyle03 = ['font-weight: bold', 'color: blue', 'background-color: #f5f5f5'].join(';');

export const consoleStyle04 = ['font-weight: bold', 'color: red', 'background-color: #f5f5f5'].join(';');

/*Cookies*/
export const getCookies = (name: string) => {
    return Cookies.get(name);
};

export const setCookies = (name: string, value: string, option?: CookieAttributes) => {
    Cookies.set(name, value, option);
};

export const removeCookies = (name: string, option?: CookieAttributes) => {
    Cookies.remove(name, option);
};

export const getBearerToken = () => {
    return getCookies(Config.BEARERTOKEN);
};

/*sessionStorage*/
export const addStorage = (name: string, value: string) => {
    sessionStorage.setItem(name, value);
};

export const getStorage = (name: string) => {
    return sessionStorage.getItem(name);
};

export const removeStorage = (name: string) => {
    sessionStorage.removeItem(name);
};

/*createAccessToken*/
export const createAccessToken = (token: string) => {
    const date = new Date();
    date.setTime(date.getTime() + Config.COOKIES_TIME);
    setCookies(Config.BEARERTOKEN, token, { expires: date });
};

/*특수문자 삭제 함수*/
export const regExp = (str: string) => {
    const reg = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
    //특수문자 검증
    if (reg.test(str)) {
        //특수문자 제거후 리턴
        return str.replace(reg, '');
    } else {
        //특수문자가 없으므로 본래 문자 리턴
        return str;
    }
};

/*rem 폰트 계산*/
export const remCaculate = () => {
    const doc = document.documentElement;
    let fontSizeVal = parseInt(((doc.clientWidth / 375) * 62.5 * 10).toString()) / 10;
    fontSizeVal = fontSizeVal >= 125 ? 125 : fontSizeVal;

    doc.style.fontSize = fontSizeVal + '%';
};
