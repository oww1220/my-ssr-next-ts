const { createServer } = require('https');
const { parse } = require('url');
const { readFileSync } = require('fs');
const next = require('next');

const port = 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

//openSSL로 개인키, 인증서 생성해서 경로에 넣음!
const httpsOptions = {
    key: readFileSync('./certificates/key.pem'),
    cert: readFileSync('./certificates/cert.pem')
};

app.prepare()
    .then(() => {
    createServer(httpsOptions, (req, res) => {
        const parsedUrl = parse(req.url, true);
        handle(req, res, parsedUrl);
    }).listen(port, err => {
        if (err) throw err;
        console.log(`> Ready on https://localhost:${port}`);
    })
});