This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.

## 리액트 패턴 - 3가지다 기본적으로 컴포넌트 상태 관리 로직을 공유하기 위한 쓰는 방법임
`고차컴퍼넌트(Higher-Order Components)`: 주로 클레스 컴퍼넌트에서<br/>
구체적으로 고차 컴포넌트는 컴포넌트를 취하여 새로운 컴포넌트를 반환하는 함수임.<br/>
반복적인 로직을 고차컴포넌트를 이용해 추상화하면 공통기능을 분리할 수 있음, 즉.`공통기능을 분리하기 위해 사용함`<br/>
고차컴포넌트를 너무 사용하다보면 렌더링성능에 좋지않고 리액트 개발자툴에서 디버깅시에도 불편해집니다. 그리고 정적메서드는 출력되는 컴포넌트에 전달되지 않습니다.<br/>
<br/>
`컴퍼넌트 합성(Provider 패턴)`:
상속은 재사용의 관점보다는 기능 확장에 사용<br/>
재사용을 하기 위해서는 합성(Composition) has-a 방식을 사용.<br/>
마운트될시에! `모든 컴퍼넌트들이 거쳐가는 컴퍼넌트`! <br/>
공통 로직을 넣음<br/>
<br/>
`리액트 훅(React Hooks)-커스텀 훅`: 주로 함수 컴퍼넌트에서<br/>
커스텀 훅은 컴포넌트 분할과는 달리 `‘컴포넌트 로직 자체를 분할하거나 재사용’`<br/>
커스텀 훅은 컴포넌트를 분할하지 않고 로직만을 재사용 가능한 형태로 뽑아서 캡슐화할 수 있다는 점이 장점임<br/>
즉 위의 고차컴퍼넌트와 사용의의는 같음!

## release
릴리스 푸쉬 도와주는 패키지
`npm i -D release-it`<br/>
설치및 스크립트 설정!<br/>
"scripts": {<br/>
    "dev": "next dev",<br/>
    "build": "next build",<br/>
    "start": "next start",<br/>
    `"release": "release-it"`<br/>
},<br/>

release 명령어<br/>
`npm run release`

## Prettier , ESLint
`npm i -D eslint eslint-config-airbnb eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react eslint-plugin-react-hooks`<br />
eslint-plugin-import : ES6 의 import/export syntax 체크, 파일 경로나 import 이름을 잘못 입력하는지 여부를 체크해주는 lint 플러그인입니다.<br />
eslint-plugin-jsx-a11y : 리액트 element 의 접근성 이슈를 짚어 lint 해주는 플러그인입니다. 예를 들자면 interactive 하지 않은 엘리먼트에 click 핸들러가 달려 있다. 이런 오류를 뿜습니다.<br />
eslint-plugin-react : 리액트 규칙들을 추가해주는 플러그인입니다.<br />
eslint-plugin-import : 리액트 hooks 규칙들을 추가해 주는 플러그인이겠죠?<br />
<br />
`npm i -D prettier eslint-config-prettier eslint-plugin-prettier`<br />
eslint-config-prettier : ESLint의 formatting 관련 설정 중 Prettier와 충돌하는 부분을 비활성화합니다. Prettier 에서 문법 관련된 ESlint 규칙을 사용하지 않게 되기 때문에 ESLint 는 자바스크립트 문법을 담당하고, 코드 스타일 정리는 Prettier 가 담당하게 됩니다.<br />
eslint-plugin-prettier : 원하는 형식의 formatting 을 설정해줍니다.<br />
<br />
`npm i -D babel-eslint eslint-plugin-babel` <br />
babel-eslint : Babel 이 서포트해주는 코드에 ESLint 를 적용할 수 있도록 해줍니다. 한마디로, ES6 이상의 자바스크립트, flow type 등의 feature 를 지원해줍니다. ESLint 만 깔 경우, ES6 / JSX / 객체 rest, spread 까지의 문법만 지원해줍니다. 그 이상의 문법 지원이 필요할 경우 반드시 이걸 깔아줘야 하는 거죠.<br />
eslint-plugin-babel : 더 많은, 실험중인 feature 까지 지원해주는 플러그인입니다. babel-eslint 를 보완해주는 플러그인입니다.<br />
<br />
`.eslintrc.json, .prettierrc` 입맛에 맛게 설정!

## Lint-Staged
린트(lint) 또는 린터(linter)는 소스 코드를 분석하여 프로그램 오류, 버그, 스타일 오류, 의심스러운 구조체에 표시(flag)를 달아놓기 위한 도구들을 가리킨다.<br/>
lint를 staged에 있는 파일을 체크함으로서, lint:fix를 통과하지 않는 파일은 커밋 하지 않게 도와줍니다.<br />
설치<br />
`npm i --save-dev lint-staged@next`<br/>
`yarn lint-staged` 로 수동실행

## Husky
githooks 를 npm 을 통해서 관리할 수 있게 도와주는 라이브러리 입니다.<br />
최신버전은 오류가 있어서 4버전때로 설치<br />
`npm i --save-dev husky@4`

### staging에 있는 파일을 npm 으로 다루기 위해서 → lint-staged
### git hooks를 npm으로 다루기 위해서 → husky 를 이용한다. 
### 이 두가지를 조합해서 commit 하기 전에 파일들을 lint 룰이 적용된 파일만 커밋할 수 있게 도와줍니다.
<br />

## base url 설정
tsconfig.json 에 `"baseUrl": "./src"` 추가

## next env 
`.env` : 기본 맵핑 환경파일<br/>
`.env.development` : dev 일경우 .env에 덮혀짐<br/>
`.env.production` : build 일경우 .env에 덮혀짐<br/>
`.env.local` : 서버에만 노출되는 .env에 덮혀짐

## 웹폰트 및 img 및 scss 설정

# 웹폰트
- `npm i -D next-fonts`
- `next.config.js` 설정파일 추가!

# img
- `npm i -D next-images`
- `next.config.js` 설정파일 추가!
- next-env.d.ts 에 - 하단 타입 참조 추가<br/>
`/// <reference types="next-images" />`

# scss
- `npm i -D sass`
- `next.config.js` 설정파일 추가!

# postcss
- `npm i -D postcss-pxtorem cssnano@4.0.1`
- `postcss.config.js` 설정파일 추가! 

# css autoprefixer
 - package.json 에 해당항목 추가!: <br/>
`"browserslist": ["last 2 versions", "> 1%"],`

## redux, redux-saga
next-redux-wrapper: next용 ssr csr store 적용 패키지<br/>
react-redux: redux<br/>
redux-saga: redux-saga<br/>
redux-devtools-extension: redux 크롬 디버깅툴<br/>

`npm i next-redux-wrapper react-redux redux-saga redux-devtools-extension`<br/>
`npm i @types/react-redux @types/redux-devtools-extension -D`

## GraphQL, apollo 
graphql: graphql 패키지 - 아폴로는 이 패키지 기반으로 작동됨<br/>
@apollo/client: React 앱에 Apollo Client를 사용하게 해서 graphql을 적용시킴<br/>
`npm i @apollo/client graphql`<br/>

## react-hook-form 
react-hook form 패키지<br/>
`npm i react-hook-form`

## classnames
classnames 조건부 처리 패키지<br/>
`npm i classnames`<br/>
`npm i @types/classnames --save-dev`

## jwt-decode, js-cookie, cookie
jwt-decode :json web tokken parser!!<br/>
`npm i jwt-decode`<br/><br/>
js-cookie: cookie 생성 삭제 <br/>
`npm i js-cookie`<br/>
`npm i @types/js-cookie --save-dev`<br/><br/>
cookie: string cookie --> 객체화<br/>
`npm i cookie`<br/>
`npm i @types/cookie --save-dev`

## bundle-analyzer!!
@next/bundle-analyzer: bundle-analyzer 는 빌드된 파일을 트리맵 형식으로 어떤것들이 있는지 크기는 어느정도 되는지 알려주는 아주 유용한 플러그인 입니다. <br/>
다운후 next.config.js에 설정<br/>
`npm i @next/bundle-analyzer`<br/>

cross-env: 는 운영체제나 플랫폼에 종속되지 않고 동일한 방법으로 env 변수를 주입하는 방법이다.<br/>
cross-env 패키지를 사용하면 동적으로 process.env(환경 변수)를 변경할 수 있습니다. 또한, 모든 운영체제에서 동일한 방법으로 환경 변수를 변경할 수 있게 됩니다.<br/>
`npm i -d cross-env `


## jest 설정!
next.js는 jest가 설치가 되어 있지않기 때문에 직접 설치를 해야된다 <br/>

1. 패키지 설치<br/>
`npm i -D babel-jest jest @types/jest @testing-library/react @testing-library/jest-dom @types/testing-library__jest-dom @testing-library/user-event @types/testing-library__user-event @testing-library/dom`

2. create `.babelrc` files and add to the babel config<br/>
{<br/>
  "presets": ["next/babel"]<br/>
}

3. create `jest.config.js` and `jest.setup.ts` files<br/>
// jest.config.js<br/>
module.exports = {<br/>
  setupFilesAfterEnv: ['<rootDir>/jest.setup.ts'],<br/>
  testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],<br/>
};<br/><br/>
// jest.setup.ts<br/>
import '@testing-library/jest-dom';<br/>

4. if `.eslintrc.json`  Exist? add config<br/>
"env": {<br/>
    "browser": true,<br/>
    "es6": true,<br/>
    "node": true,<br/>
    `"jest": true`<br/>
},<br/>

5. add scripts test!<br/>
"scripts": {<br/>
    "dev": "next dev",<br/>
    "build": "next build",<br/>
    "start": "next start",<br/>
    `"test": "jest --watch"`,<br/>
    "release": "release-it"<br/>
},

6. `yarn test`


## redux-saga-test-plan
redux-saga-test-plan은 사가(Saga) 제너레이터 함수를 테스트할 때, 실제 구현 로직과 테스트 코드가 갖는 커플링, 그리고 매뉴얼 한 테스트에 대한 문제를 해결해준다. <br />
테스트에 선언적이고, 체이닝(chainable) API를 제공해서 실제 구현체인 사가에서 원하는 이펙트만을 테스트할 수 있도록 도와준다. <br />
이외 어떤 이펙트들을 나타내는지, 이펙트들의 순서는 어떻게 되는지 신경쓰지 않고 걱정하지 않도록 한다. <br />
redux-saga의 런타임을 함께 사용하므로, 통합 테스트를 할 수도 있고, redux-saga-test-plan에 내장된 이펙트 목킹(mocking)을 활용해 유닛 테스트도 작성할 수 있다.<br />
`npm i -D redux-saga-test-plan`

## redux-mock-store
redux-mock-store는 상태를 업데이트 하지 않고 디스패치에 전달된 작업만 기록하기 때문에, store.dispatch() 구문 실행 후 store.getState()로 변경된 상태를 확인하는 것이 불가능합니다.<br />
해당 라이브러리가 리듀서 관련 로직이 아닌 액션 관련 로직을 테스트하도록 설계되었기 때문에 이는 어쩔 수 없는 부분이 아닌가 생각합니다.<br />
`npm i -D redux-mock-store`<br />
`npm i -D @types/redux-mock-store`


## 타입스크립트 패키지 적용시
https://www.typescriptlang.org/dt/search?search=<br/>
에서 패키지 찾아서 설치

## 도커 명령어

<빌드 명령어-이미지 생성><br />
prod(Dockerfile.prod):<br />
`docker build -t my-ssr-next-prod:1.0.0 -f Dockerfile.prod .`<br />
dev(Dockerfile):<br />
`docker build -t my-ssr-next-dev:1.0.0 .`<br />

<런 명령어-이미지기반 컨테이너 생성><br />
prod:<br />
`docker run -p 83:3000 --name my-ssr-next-prod my-ssr-next-prod:1.0.0`<br />
dev:<br />
`docker run -p 83:3000 --name my-ssr-next-dev my-ssr-next-dev:1.0.0`<br />

*도커 내부 컨테이너 안에서 <호스트 localhost>는 접근이 안됨,<br/>
도커를 깔면 호스트 파일에 host.docker.internal 세팅이 자동으로 됨 <br/>
<br/>
Docker Compose란<br/>
복수 개의 컨테이너를 실행시키는 도커 애플리케이션이 정의를 하기 위한 툴입니다.<br/>
Compose를 사용하면 YAML 파일을 사용하여 애플리케이션의 서비스를 구성할 수 있습니다.<br/>
docker-compose 로 해결은 가능할듯하나 테스트는 안해봄.. 아직 app서버는 도커이미자화 안시킴 <br/>
docker-compose.yml - 실행내용 : (nginx서버는 reverse Proxy 서버로 다른포트의 express서버를 연결해주고 express서버를 감추어준다!)<br/>
docker-compose.yml 내용중 #도커 이미지 업데이트시 이미지 버전 동기화 필요!<br/>
client:<br/>
    image: `my-ssr-next-ts:1.0.0 `<br/>

실행문!<br/>
prod:<br />
`docker-compose -f docker-compose-prod.yml up -d` <br />
dev:<br />
`docker-compose up -d`


## 호스트 정보(호스트 파일) 
서버 컴퓨터 ip 적용(예시부분) <br />
172.28.1.73(was서버 ip) lapi.react-ssr.com<br />
`react-test-ssr`<br />
`172.28.1.73 lapi.react-ssr.com`<br />
`127.0.0.1 lwww.react-ssr.com`


## custom server scripts(https를 사용하기 위해 커스텀함!)
`<일반: http>`<br />
dev url:<br />
http://lwww.react-ssr.com:3000
"dev": "next dev", <br />
"build": "next build", <br />
"start": "next start", <br />
<br />
`<커스텀: https--test용임!>` <br />
dev url:<br />
https://lwww.react-ssr.com:3000/
"dev": "cross-env NODE_TLS_REJECT_UNAUTHORIZED='0' node server.js", <br />
"build": "next build", <br />
"start": "cross-env NODE_ENV=production NODE_TLS_REJECT_UNAUTHORIZED='0' node server.js", <br />

## openssl 명령어
genrsa  -out `private.key` 2048 : private.key 생성(개인키 발급)<br />
rsa -in `private.key` -pubout -out `public.key` : private.key 기반 public.key 생성(공개키 발급)<br />
req -config ./openssl.cnf -new -key `private.key` -out `private.csr` : CSR( 인증요청서 ) 만들기<br />
req -config ./openssl.cnf -x509 -days 365 -key `private.key` -in `private.csr` -out `mycommoncrt.crt` -days 365 : CRT( 인증서 ) 만들기<br />
x509 -in `mycommoncrt.crt` -out `mycommonpem.pem` -outform PEM : CRT( 인증서 ) pem 파일로! <br />
rsa -in `private.key` -out `private.pem` -outform PEM : 개인키 pem 파일로! <br />

## 연결 url
도커 ssl 연결<br />
docker dev url: http로 연결된 포트 연결시에 https서버로 리다이랙트시킴!<br />
http://lwww.react-ssr.com:83/<br />
https://lwww.react-ssr.com/ <br />





